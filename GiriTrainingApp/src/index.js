/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, View} from 'react-native';

import Tugas1 from './screens/Tugas1/Tugas1';
import {Tugas2} from './screens/Tugas2';
import Tugas3 from './screens/Tugas3/TodoList';
import Tugas4 from './screens/Tugas4';
const App = () => {
  return (
    <View style={styles.container}>
      <Tugas4 />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
