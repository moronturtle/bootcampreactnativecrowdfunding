import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Tugas2 = () => {
  return (
    <View style={styles.Container}>
      {/* Header */}
      <View style={styles.Header}>
        <Text style={styles.textHeader}>Account</Text>
      </View>

      {/* Profile */}
      <View style={styles.profileName}>
        <Image
          style={styles.profileNameImage}
          source={{
            uri:
              'https://football-tribe.com/asia/wp-content/uploads/sites/8/2017/04/Park-Ji-Sung-800x517.jpg',
          }}
        />
        <Text style={styles.profileNameText}>Hilman Giri </Text>
      </View>

      <View style={styles.hrLine} />

      {/* Dana Atau Saldo */}
      <View style={styles.DanaContainer}>
        <Icon name="credit-card" size={30} color="grey" />
        <Text style={styles.itemText}>Saldo</Text>
        <Text style={styles.itemText2}>Rp. 120.000.000</Text>
      </View>

      <View style={styles.hrLine2} />

      {/* kolom container untuk kebawah */}
      <View style={{flexDirection: 'column'}}>
        <View style={styles.TextContainer}>
          <Icon name="cogs" size={30} color="grey" />
          <Text style={styles.itemText}>Pengaturan</Text>
        </View>

        <View style={styles.hrLine} />

        <View style={styles.TextContainer}>
          <Icon name="question-circle" size={30} color="grey" />
          <Text style={styles.itemText}>Bantuan</Text>
        </View>

        <View style={styles.hrLine} />

        <View style={styles.TextContainer}>
          <Icon name="fax" size={30} color="grey" />
          <Text style={styles.itemText}>Syarat Ketentuan</Text>
        </View>

        <View style={styles.hrLine2} />

        <View style={styles.TextContainer}>
          <Icon name="door-open" size={30} color="grey" />
          <Text style={styles.itemText}>Keluar</Text>
        </View>

        <View style={styles.hrLine} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {},
  Header: {
    flex: 1,
    backgroundColor: 'blue',
    padding: 25,
    paddingBottom: 45,
  },
  textHeader: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
  },
  profileName: {
    backgroundColor: 'white',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    padding: 25,
  },
  profileNameText: {
    marginTop: 20,
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },
  profileNameImage: {
    height: 65,
    width: 65,
    borderRadius: 40,
  },
  hrLine: {
    backgroundColor: 'grey',
    opacity: 0.1,
    height: 2,
  },
  hrLine2: {
    backgroundColor: 'grey',
    opacity: 0.2,
    height: 5,
  },
  DanaContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    padding: 25,
  },
  TextContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    padding: 25,
  },
  itemText: {
    marginLeft: 20,
    marginVertical: 5,
    fontSize: 15,
  },
  itemText2: {
    marginLeft: 20,
    marginVertical: 5,
    fontSize: 15,
    marginLeft: 'auto',
  },
});

export default Tugas2;
