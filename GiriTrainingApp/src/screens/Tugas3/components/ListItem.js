import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome/';

const ListItem = ({item, deleteItem}) => {
  return (
    <View
      style={{
        marginTop: 20,
        borderColor: 'grey',
        borderWidth: 0.7,
        marginLeft: 10,
        marginRight: 25,
      }}>
      <View style={styles.listItemDate}>
        <Text>{item.date}</Text>
      </View>
      <View style={styles.listItemView}>
        <Text style={styles.listItemText}>{item.text}</Text>
        <Icon
          name="trash"
          size={20}
          color="black"
          onPress={() => deleteItem(item.id)}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  listItemDate: {
    marginBottom: -20,
    marginLeft: 25,
    marginTop: 15,
  },
  listItem: {
    margin: 20,
    padding: 15,
    backgroundColor: '#f8f8f8',
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  listItemView: {
    margin: 25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  listItemText: {
    fontSize: 18,
  },
});

export default ListItem;
