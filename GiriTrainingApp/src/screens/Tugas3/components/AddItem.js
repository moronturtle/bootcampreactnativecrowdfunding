import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome/';

const AddItem = ({addItem}) => {
  const [text, setText] = useState();
  const onChange = (textValue) => {
    setText(textValue);
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Add Items"
        style={styles.input}
        onChangeText={onChange}
      />
      <TouchableOpacity
        style={styles.btn}
        onPress={() => {
          addItem(text);
          setText('');
        }}>
        <Icon name="plus" size={20} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginLeft: 5,
  },
  input: {
    width: '80%',
    padding: 8,
    margin: 5,
    borderColor: 'black',
    borderWidth: 2,
  },
  btn: {
    backgroundColor: '#c2bad8',
    padding: 9,
    margin: 5,
  },
});

export default AddItem;
