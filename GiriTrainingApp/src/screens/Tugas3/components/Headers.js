import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

const Headers = ({}) => (
  <View style={styles.container}>
    <Text>Masukan Todo List</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    margin: 20,
    fontSize: 15,
  },
});

export default Headers;
