import React, {useState} from 'react';
import {Text, View, StyleSheet, FlatList, Alert} from 'react-native';
import {v4 as uuid} from 'uuid';

import Headers from './components/Headers';
import AddItem from './components/AddItem';
import ListItem from './components/ListItem';

const TodoList = () => {
  const [items, setItems] = useState([
    {id: uuid(), text: 'Milk', date: '9/6/2020'},
    {id: uuid(), text: 'Eggs', date: '9/6/2020'},
  ]);

  const deleteItem = (id) => {
    setItems((previousItem) => {
      return previousItem.filter((item) => item.id != id);
    });
  };

  const addItem = (item) => {
    setItems((previousItem) => {
      let d = new Date();
      let date = `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`;
      return [{id: uuid(), text: item, date: date}, ...previousItem];
    });
  };

  return (
    <View>
      <Headers />
      <AddItem addItem={addItem} />
      <FlatList
        data={items}
        renderItem={({item}) => (
          <ListItem item={item} deleteItem={deleteItem} />
        )}
      />
    </View>
  );
};

export default TodoList;
