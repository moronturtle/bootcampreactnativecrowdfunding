import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Tugas1 = ({params}) => (
  <View style={styles.container}>
    <Text style={styles.text}>
      Hello Kelas React Native Lanjutan SanberCode !!
    </Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Text: {},
});

export default Tugas1;
