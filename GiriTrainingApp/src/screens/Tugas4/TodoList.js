import React, {useState, useContext} from 'react';
import {Text, View, StyleSheet, FlatList, Alert} from 'react-native';
import {v4 as uuid} from 'uuid';

import {RootContext} from './index';
import Headers from './components/Headers';
import AddItem from './components/AddItem';
import ListItem from './components/ListItem';

const TodoList = () => {
  const state = useContext(RootContext);

  return (
    <View>
      <Headers />
      <AddItem
        addItem={state.addTodo}
        valueInput={state.input}
        handleChangeInput={state.handleChangeInput}
      />
      <FlatList
        data={state.todos}
        renderItem={({item}) => (
          <ListItem item={item} deleteItem={state.deleteTodo} />
        )}
      />
    </View>
  );
};

export default TodoList;
