import React, {useState, createContext} from 'react';
import {v4 as uuid} from 'uuid';

import TodoList from './TodoList';

export const RootContext = createContext();

const Context = ({params}) => {
  const [input, setInput] = useState('');
  const [todos, setTodos] = useState([]);

  const handleChangeInput = (value) => {
    setInput(value);
  };

  const addTodo = (item) => {
    let d = new Date();
    let date = `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`;
    setTodos([{id: uuid(), text: input, date: date}, ...todos]);
    setInput('');
  };

  const deleteTodo = (id) => {
    setTodos((previousItem) => {
      return previousItem.filter((item) => item.id != id);
    });
  };

  return (
    <RootContext.Provider
      value={{input, todos, handleChangeInput, addTodo, deleteTodo}}>
      <TodoList />
    </RootContext.Provider>
  );
};

export default Context;
